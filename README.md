# Muon_GeoModelXml

from within this project:

## create an alias for gmex

alias GMX="gmex /usr/local/lib/libGMXPlugin.dylib "

## for custom colours
ln -s conf/gmexMatVisAttributes.json ~/.gmexMatVisAttributes.json

## to run 

cd xml

ln -s BML1.xml gmx.xml

GMX


## general philosophy
BML1.xml is the main XML description. It makes use of XML Entities 
(a little mind boggling to begin with but Entities work pretty much 
the same as #include or #define in the C pre-processor) for code reuse.
More station types can be described in the same manner, with the caveat
that a modification to GMXPlugin is needed to read in more than one XML 
file. Enjoy! 


